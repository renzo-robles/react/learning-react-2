import React, { useState } from "react";
import Error from "./Error";
import shortid from "shortid";
import PropTypes from "prop-types";

const Formulario = ({ agregarNuevoGasto, guardarCrearGasto }) => {
  const [nombre, setNombre] = useState("");
  const [cantidad, setCantidad] = useState(0);
  const [error, setError] = useState(false);

  const agregarGasto = (e) => {
    e.preventDefault();
    if (cantidad <= 0 || isNaN(cantidad) || nombre.trim() === "") {
      setError(true);
      return;
    } else {
      setError(false);

      const gasto = {
        nombre,
        cantidad,
        id: shortid.generate(),
      };

      agregarNuevoGasto(gasto);
      guardarCrearGasto(true);
      setNombre("");
      setCantidad(0);
    }
  };

  return (
    <form onSubmit={agregarGasto}>
      <div>
        <h2>Agregar tus gastos aqui</h2>
        {error ? (
          <Error mensaje="Ambos campos son obligatorios o Presupuesto incorrecto" />
        ) : null}

        <div className="campo">
          <label>Nombre gasto</label>
          <input
            type="text"
            className="u-full-width"
            placeholder="Ej. Transporte"
            value={nombre}
            onChange={(e) => setNombre(e.target.value)}
          ></input>
        </div>

        <div className="campo">
          <label>Cantidad gasto</label>
          <input
            type="number"
            className="u-full-width"
            placeholder="Ej. 300"
            value={cantidad}
            onChange={(e) =>
              e.target.value !== ""
                ? setCantidad(parseInt(e.target.value))
                : setCantidad(e.target.value)
            }
          ></input>
        </div>

        <input
          type="submit"
          className="button-primary u-full-width"
          value="Agregar gasto"
        />
      </div>
    </form>
  );
};

Formulario.propTypes = {
  agregarNuevoGasto: PropTypes.func.isRequired,
  guardarCrearGasto: PropTypes.func.isRequired,
};

export default Formulario;
